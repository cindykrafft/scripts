import json,httplib,urllib2,csv,time
from parse_rest.connection import register as parse_register
from parse_rest.datatypes import Object as parse_Object
from datetime import timedelta, datetime, date
import requests

class SleepData(parse_Object):
	pass

class ParseData:

	def __init__(self, start_date, end_date):
		self.__registerParseKey()
		self.sleep_data = self.retrieve_sleep_logs(start_date, end_date)

	def __getParseKeys_productionapp(self):
		application_id = "Cnragz6DaES2WMMTVBYOkATNS7BJPBxRwq5JfJzy"
		rest_api_key = "UXgUcy10qT1SIEPn2WKujnoo6YCtSdwm89fmaupO"
		return application_id, rest_api_key

	def __registerParseKey(self):
		[application_id, rest_api_key] = self.__getParseKeys_productionapp();
		parse_register(application_id, rest_api_key)

	def retrieve_sleep_logs(self, start_date, end_date):
		sleep_logs = []
		queryLimit = 1000 # max allowable from Parse
		maxSkippingAllowed = 10000 # max allowable from Parse
		while True:
			try:
				post = SleepData.Query.filter(createdAt__gte=start_date).filter(createdAt__lte=end_date).order_by("-createdAt")
				totalCount = post.count() # total number of objects, we need to break this up
				print "totalCount = %d" % totalCount
				retrieveCount = 0
				while (retrieveCount < totalCount):
					ml = post.skip(retrieveCount).limit(queryLimit)
					retrieveCount = retrieveCount + len(ml)
					sleep_logs.extend(ml)
					if (retrieveCount >= (maxSkippingAllowed - queryLimit)):
						lastCreatedAt = sleep_logs[-1].createdAt
						print "new post (curr=%d, len=%d, total=%d) --> last %s" % (retrieveCount, len(sleep_logs), totalCount, lastCreatedAt)
						post = SleepData.Query.filter(createdAt__gte=start_date).filter(createdAt__lte=end_date).filter(createdAt__lt=lastCreatedAt).order_by("-createdAt")
						retrieveCount = 0
						totalCount = post.count()
				print "sleep logs collected (%d)" % len(sleep_logs)
				return sleep_logs
			except Exception, e:
				print e
				time.sleep(2)
				continue
			break

start_date = datetime.strptime("2015-2-11", "%Y-%m-%d")
end_date = datetime.strptime("2015-2-12", "%Y-%m-%d")
while start_date < datetime.utcnow():
	print "processing day: " + str(start_date)
	parse_data = ParseData(start_date, end_date)
	user_to_start_time_to_log_mapping = {}
	for item in parse_data.sleep_data:
		if item.userId not in user_to_start_time_to_log_mapping:
			user_to_start_time_to_log_mapping[item.userId] = {}
		if item.startTime not in user_to_start_time_to_log_mapping[item.userId]:
			to_add = [item.objectId, item.date, item.log, item.startTime, item.totalMinutes, item.typeMinutes, item.userId, item.createdAt, item.updatedAt]
			user_to_start_time_to_log_mapping[item.userId][item.startTime] = to_add
		else:
			check_added = [item.objectId, item.date, item.log, item.startTime, item.totalMinutes, item.typeMinutes, item.userId, item.createdAt, item.updatedAt]
			#check item.date
			if user_to_start_time_to_log_mapping[item.userId][item.startTime][1] > check_added[1]:
				data = {"originalObjectId":check_added[0], "date":str(check_added[1]), "log":check_added[2], "startTime":check_added[3], "totalMinutes":check_added[4], "typeMinutes":check_added[5], "userId":check_added[6], "originalCreatedAt": str(check_added[7]), "originalUpdatedAt": str(check_added[8])}
				id_to_delete = check_added[0]
			else:
				data = {"originalObjectId":item.objectId, "date":str(item.date), "log":item.log, "startTime":item.startTime, "totalMinutes":item.totalMinutes, "typeMinutes":item.typeMinutes, "userId":item.userId, "originalCreatedAt": str(item.createdAt), "originalUpdatedAt": str(item.updatedAt)}	
				id_to_delete = item.objectId
			#assert user_to_start_time_to_log_mapping[item.userId][item.startTime][1] == check_added[1]
			if user_to_start_time_to_log_mapping[item.userId][item.startTime][2] == check_added[2] and user_to_start_time_to_log_mapping[item.userId][item.startTime][3] == check_added[3] and user_to_start_time_to_log_mapping[item.userId][item.startTime][4] == check_added[4] and user_to_start_time_to_log_mapping[item.userId][item.startTime][5] == check_added[5] and user_to_start_time_to_log_mapping[item.userId][item.startTime][6] == check_added[6]:
			#if all data portions are equal to the one already in the dict, then we will put this item in the SleepDuplicates class.
				headers = {"X-Parse-Application-Id": "Cnragz6DaES2WMMTVBYOkATNS7BJPBxRwq5JfJzy", "X-Parse-REST-API-Key": "UXgUcy10qT1SIEPn2WKujnoo6YCtSdwm89fmaupO", "Content-Type": "application/json"}
				post_request = requests.post("https://api.parse.com/1/classes/SleepDuplicates", data = json.dumps(data), headers=headers)
				assert post_request.status_code == 201
				url_to_delete = "https://api.parse.com/1/classes/SleepData/" + id_to_delete
				r = requests.delete(url_to_delete, headers=headers)
				r.raise_for_status()
			else:
				f = open("weird_sleep_logs.txt", "a")
				f.write("user: " + str(item.userId) + "\t" + "startTime: " + str(item.startTime) + "\n")
				f.close()
	start_date = start_date + timedelta(days=1)
	end_date = start_date + timedelta(days=1)


