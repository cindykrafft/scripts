import json,httplib,urllib2,csv,time
from parse_rest.connection import register as parse_register
from parse_rest.datatypes import Object as parse_Object
from datetime import timedelta, datetime, date
import requests

class MessageLogs(parse_Object):
	pass

class ParseData:

	def __init__(self):
		self.__registerParseKey()
		self.message_logs = self.retrieve_message_logs()

	def __getParseKeys_productionapp(self):
		application_id = "Cnragz6DaES2WMMTVBYOkATNS7BJPBxRwq5JfJzy"
		rest_api_key = "UXgUcy10qT1SIEPn2WKujnoo6YCtSdwm89fmaupO"
		return application_id, rest_api_key

	def __registerParseKey(self):
		[application_id, rest_api_key] = self.__getParseKeys_productionapp();
		parse_register(application_id, rest_api_key)

	def retrieve_message_logs(self):
		sleep_logs = []
		queryLimit = 1000 # max allowable from Parse
		maxSkippingAllowed = 10000 # max allowable from Parse
		start_date = datetime.utcnow() - timedelta(days=1) #scan message logs from the past day
		while True:
			try:
				post = MessageLogs.Query.filter(createdAt__gte=start_date).order_by("-createdAt")
				totalCount = post.count() # total number of objects, we need to break this up
				print "totalCount = %d" % totalCount
				retrieveCount = 0
				while (retrieveCount < totalCount):
					ml = post.skip(retrieveCount).limit(queryLimit)
					retrieveCount = retrieveCount + len(ml)
					sleep_logs.extend(ml)
					if (retrieveCount >= (maxSkippingAllowed - queryLimit)):
						lastCreatedAt = sleep_logs[-1].createdAt
						print "new post (curr=%d, len=%d, total=%d) --> last %s" % (retrieveCount, len(sleep_logs), totalCount, lastCreatedAt)
						post = MessageLogs.Query.filter(createdAt__gte=start_date).filter(createdAt__lt=lastCreatedAt).order_by("-createdAt")
						retrieveCount = 0
						totalCount = post.count()
				print "sleep logs collected (%d)" % len(sleep_logs)
				return sleep_logs
			except Exception, e:
				print e
				time.sleep(2)
				continue
			break

parse_data = ParseData()
message_logs = parse_data.message_logs
list_of_user_ids = []
for item in message_logs:
	try:
		user_id = item.userId
	except:
		try:
			user_id = item.User_ID
		except:
			continue
	if user_id not in list_of_user_ids:
		try:
			text_file = item.messageFile
			url = text_file._file_url
			data = urllib2.urlopen(url)
		except:
			continue
		for line in data:
			if "ERR:ACCEL" in line:
				list_of_user_ids.append(user_id)
print list_of_user_ids
