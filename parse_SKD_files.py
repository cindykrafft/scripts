import os
from binascii import unhexlify
from struct import unpack
from string import printable

def separate_four_minute_data(byte):
	minutes = ""
	minutes += str(byte>>6)
	minutes += str(byte>>4 & 0b0011)
	minutes += str(byte>>2 & 0b000011)
	minutes += str(byte & 0b00000011)
	return minutes

def unpack_hex_bytes(f, number_of_bytes, format):
	number_of_bytes_to_read = number_of_bytes * 2
	data = ""
	attempts = 0
	while len(data) < number_of_bytes_to_read and attempts < 5:
		data += f.read(number_of_bytes_to_read - len(data))
		attempts += 1
	return unpack(format, unhexlify(data))[0]

def extract_cumulative_file_minutes(f):
	minuteOffset = unpack_hex_bytes(f, 2, "<H")
	stairs = unpack_hex_bytes(f, 1, "B")
	walk = unpack_hex_bytes(f, 1, "B")
	run = unpack_hex_bytes(f, 1, "B")
	activitySeconds = unpack_hex_bytes(f, 1, "B")
	kcal = unpack_hex_bytes(f, 1, "B")
	distance = unpack_hex_bytes(f, 1, "B")
	return [("minuteOffset", minuteOffset), ("stairs", stairs), ("walk", walk), ("run", run), ("activitySeconds", activitySeconds), ("kcal", kcal), ("distance", distance)]


class ParsedPedometerStateFile:

	def __init__(self, filename):
		self.is_correct_length(filename)
		self.decoded_file = self.parse_pedometer_state_file(filename)

	def is_correct_length(self, filename):
		with open(filename, 'r') as data:
			if os.stat(filename).st_size == 720:
				return
			else:
				raise Exception("Pedometer state file is incorrect size")

	def parse_pedometer_state_file(self, filename):
		with open(filename, 'r') as data:
			minutes = ""
			position = 0
			while position < 360:
				current_byte = ""
				while len(current_byte) < 2:
					current_byte += data.read(2 - len(current_byte))
				current_byte = int(ord(unhexlify(current_byte)))
				current_byte = separate_four_minute_data(current_byte)
				minutes += current_byte
				position += 1
			return minutes


class ParsedSleepLogFile:

	def __init__(self, filename):
		self.decoded_file = self.parse_sleep_log(filename)

	def parse_sleep_log(self, filename):
		with open(filename, 'r') as data:
			data_length = os.stat(filename).st_size
			minutes = ""
			position = 0
			while position < data_length/2:
				current_byte = ""
				while len(current_byte) < 2:
					current_byte += data.read(2 - len(current_byte))
				current_byte = int(ord(unhexlify(current_byte)))
				current_byte = separate_four_minute_data(current_byte)
				minutes += current_byte
				position += 1
			return minutes


class ParsedSurveyResponseFile:

	def __init__(self, filename):
		self.is_correct_length(filename)
		self.decoded_file = self.parse_survey_response_file(filename)

	def is_correct_length(self, filename):
		with open(filename, 'r') as data:
			if os.stat(filename).st_size % 74 == 0:
				return
			else:
				raise Exception("Survey response file is incorrect size")

	def parse_survey_response_file(self, filename):
		with open(filename, 'r') as data:
			survey_response_file = []
			file_length = os.stat(filename).st_size
			current_position = 0
			while current_position < file_length/74:
				notification_id = unpack_hex_bytes(data, 4, "<I")
				survey_response = unpack_hex_bytes(data, 1, "b")
				survey_choices = ""
				while len(survey_choices) < 64:
					survey_choices += data.read(64 - len(survey_choices))
				print survey_choices
				survey_choices = unpack("32c", unhexlify(survey_choices))
				survey_choices_padding_start = survey_choices.index("\x00")
				survey_choices = survey_choices[:survey_choices_padding_start]
				results = {"notification_id":notification_id, "survey_response": survey_response, "survey_choices":"".join(survey_choices)}
				survey_response_file.append(results)
				current_position += 1
			return survey_response_file


class ParsedMessageLogFile:

	def __init__(self, filename):
		self.decoded_file = self.convert_message_log(filename)

	def is_ascii(self, data):
		count = 0
		for line in data:
			if not all(c in printable for c in line):
				return False
		return True

	def identify_corrupt_message_log(self, filename):
		count = 0
		data = open(filename, 'r')
		if self.is_ascii(data) == False:
			return "corrupt"
		else:
			return "not corrupt"

	def convert_message_log(self, filename):
		data = open(filename, 'r')
		converted_log = ""
		while True:
			try:
				c = unpack("c",unhexlify(data.read(2)))
				converted_log += c[0]
			except Exception:
				return converted_log

	def parse_message_log(self, filename):#not done yet
		date_to_steps_mapping = {}
		data = self.convert_message_log(filename)
		print data
		if "local_day_rollover" in data:
			try:
				day_end = line.index(",")
				day = line[:day_end]
				test = datetime.strptime(day, "%Y-%m-%d")
			except Exception, e:
				day_line = data.next()
				while "new_day" not in day_line:
					day_line = data.next()
				day_end = day_line.index(",")
				day = day_line[:day_end]
			start_index = line.index("local_day_rollover") + len("local_day_rollover:")
			substring = line[start_index:]
			end_index = substring.index("[")
			number_of_steps = substring[:end_index]
			date_to_steps_mapping[day] = number_of_steps
		return date_to_steps_mapping

class ParsedCumulativeActivityFile:

	def __init__(self, filename):
		#self.error_code = self.test_for_minute_errors(filename)
		self.decoded_file = self.parse_cumulative_activity_file(filename)

	def test_for_minute_errors(self, filename):
		error = 0
		data = open(filename, 'r')
		minute_length = os.stat(filename).st_size - (50 * 16)
		if minute_length % 8 != 0:
			error = error | 1
		data.read(50 * 16)
		previous_minute = 0
		first_minute_marker = True
		count = 0
		while True:
			try:
				count += 1
				minute_unpacked = extract_cumulative_file_minutes(data)
				print minute_unpacked
				if minute_unpacked[0][1] != previous_minute + 1 and not first_minute_marker:
					error = error | 2
				if minute_unpacked[4][1] > 90:
					error = error | 4
				if minute_unpacked[2][1] > 255:
					error = error | 8
				if minute_unpacked[3][1] > 255:
					error = error | 16
				if minute_unpacked[1][1] > 255:
					error = error | 32
				first_minute_marker = False
				previous_minute = minute_unpacked[0][1]
			except Exception, e:
				print e
				return error
				break

	def parse_cumulative_activity_file(self, filename):
		cumulative_activity_file = {}
		cumulative_activity_file["headers"] = []
		cumulative_activity_file["minute_blocks"] = []
		data = open(filename, 'r')
		preface = dict(self.extract_preface(data))
		cumulative_activity_file["preface"] = preface
		for i in range(48):
			header = dict(self.extract_header(data))
			cumulative_activity_file["headers"].append(header)
		UTC_total_header = dict(self.extract_header(data))
		cumulative_activity_file["UTC_total_header"] = UTC_total_header
		count = 0
		while True:
			try:
				minutes = dict(extract_cumulative_file_minutes(data))
				cumulative_activity_file["minute_blocks"].append(minutes)
			except Exception, e:
				break
		return cumulative_activity_file

	def extract_preface(self, f):
		version = f.read(4)
		minute = unpack_hex_bytes(f, 1, "B")
		hour = unpack_hex_bytes(f, 1, "B")
		day = unpack_hex_bytes(f, 1, "B")
		month = unpack_hex_bytes(f, 1, "B")
		year = unpack_hex_bytes(f, 2, ">H")
		return [("version", version), ("minute", minute), ("hour", hour), ("day", day), ("month", month), ("year", year)]

	def extract_header(self, f):
		stairs = unpack_hex_bytes(f, 4, ">L")
		walk = unpack_hex_bytes(f, 4, ">L")
		run = unpack_hex_bytes(f, 4, ">L")
		seconds = unpack_hex_bytes(f, 4, ">L")
		inches = unpack_hex_bytes(f, 4, ">L")
		kcal = unpack_hex_bytes(f, 4, ">L")
		minuteStart = unpack_hex_bytes(f, 2, ">H")/8
		minutes = unpack_hex_bytes(f, 1, "B")
		flag = unpack_hex_bytes(f, 1, "B")
		return [("stairs", stairs), ("walk", walk), ("run", run), ("seconds", seconds), ("inches", inches), ("kcal", kcal), ("minuteStart", minuteStart), ("minutes", minutes), ("flag", flag)]

class ParsedActivityMinuteFile:

	def __init__(self, filename):
		self.decoded_file = self.parse_activity_minute_file(filename)

	def parse_activity_minute_file(self, filename):
		activity_minute_file = []
		data = open(filename, 'r')
		while True:
			try:
				minutes = dict(extract_cumulative_file_minutes(data))
				activity_minute_file.append(minutes)
			except:
				break
		return activity_minute_file

class ParsedActivityTotalFine:

	def __init__(self, filename):
		self.decoded_file = self.parse_activity_total_file(filename)

	def parse_activity_total_file(self, filename):
		data = open(filename, 'r')
		stairs = unpack(">I", unhexlify(data.read(8)))[0]
		walk = unpack(">I", unhexlify(data.read(8)))[0]
		run = unpack(">I", unhexlify(data.read(8)))[0]
		minutes = unpack(">I", unhexlify(data.read(8)))[0]
		distance = unpack(">I", unhexlify(data.read(8)))[0]
		calories = unpack(">I", unhexlify(data.read(8)))[0]
		steps = unpack(">I", unhexlify(data.read(8)))[0]
		return {"stairs":stairs, "walk":walk, "run":run, "minutes":minutes, "distance":distance, "calories":calories, "steps":steps}

if __name__ == "__main__":
	#print ParsedActivityTotalFine("example_activity_total2.txt").decoded_file
	#print ParsedMessageLogFile("example_message_log.txt").decoded_file
	#print ParsedActivityMinuteFile("example_activity_minute.txt").decoded_file
	print ParsedCumulativeActivityFile("/Users/cindykrafft/Downloads/cumulative_activity_2015_2_7.skd").decoded_file
	#print ParsedPedometerStateFile("example_pedo_state_file.txt").decoded_file
	#print ParsedSleepLogFile("example_sleep_log.txt").decoded_file
	#print ParsedSurveyResponseFile("example_survey_response.txt").decoded_file
	#not_corrupted = ParsedMessageLogFile("good_message_log.txt")
	#print not_corrupted.identify_corrupt_message_log("good_message_log.txt")
	#corrupted = ParsedMessageLogFile("example_corrupted_message_log.txt")
	#print corrupted.identify_corrupt_message_log("example_corrupted_message_log.txt")