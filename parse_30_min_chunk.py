import struct
from binascii import unhexlify
from io import BytesIO
from collections import OrderedDict


def unpack_hex_bytes(f, number_of_original_bytes, format):
    number_to_read = number_of_original_bytes * 2
    data = ""
    attempts = 0
    while len(data) < number_to_read and attempts < 5:
        data += f.read(number_to_read - len(data))
        attempts += 1
    return struct.unpack(format, unhexlify(data))[0]


def parse_30_min_chunk(data):
    result = OrderedDict()
    data = BytesIO(data)
    result["preface"] = dict(extract_preface(data))
    result["header"] = dict(extract_header(data))
    result["minutes"] = []
    while True:
        try:
            minutes = dict(extract_cumulative_file_minutes(data))
            result["minutes"].append(minutes)
        except struct.error: #happens at EOF
            break
    return result


def extract_preface(f):
    junk = minute = unpack_hex_bytes(f, 1, "B")
    minute = unpack_hex_bytes(f, 1, "B")
    hour = unpack_hex_bytes(f, 1, "B")
    day = int(unpack_hex_bytes(f, 1, "B"))
    month = int(unpack_hex_bytes(f, 1, "B"))
    year = int(unpack_hex_bytes(f, 2, "<H"))
    return [("minute", minute), ("hour", hour), ("day", day),
            ("month", month), ("year", year)]


def extract_header(f):
    stairs = unpack_hex_bytes(f, 4, "<L")
    walk = unpack_hex_bytes(f, 4, "<L")
    run = unpack_hex_bytes(f, 4, "<L")
    milliseconds = unpack_hex_bytes(f, 4, "<L")
    distance = unpack_hex_bytes(f, 4, "<L")
    kcal = unpack_hex_bytes(f, 4, "<L")
    minutes = unpack_hex_bytes(f, 1, "B")
    return [("stairs", stairs), ("walk", walk), ("run", run),
            ("milliseconds", milliseconds), ("distance", distance),
            ("kcal", kcal), ("minutes", minutes)]


def extract_cumulative_file_minutes(f):
    minute_index = unpack_hex_bytes(f, 2, "<H")
    stairs = unpack_hex_bytes(f, 1, "B")
    walk = unpack_hex_bytes(f, 1, "B")
    run = unpack_hex_bytes(f, 1, "B")
    seconds = unpack_hex_bytes(f, 1, "B") * 512 / 1000.0
    kcal = unpack_hex_bytes(f, 1, "B") * 16384 / 60000.0
    distance = unpack_hex_bytes(f, 1, "B") * 512 / (5280 * 12.0)
    return [("minute_index", minute_index), ("stairs", stairs), ("walk", walk),
            ("run", run), ("seconds", seconds), ("kcal", kcal),
            ("distance", distance)]

cumulative_chunk = open('cumulative_chunk.txt', 'r').read()
print parse_30_min_chunk(cumulative_chunk)