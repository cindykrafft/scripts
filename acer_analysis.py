#!/usr/bin/python
from __future__ import division
from datetime import datetime, timedelta
import MySQLdb
import csv
import math
import matplotlib.pyplot as plt
import numpy

db = MySQLdb.connect(host=<HOST>,
                    user=<USERNAME>,
                    passwd=<PASSWORD>,
                    db=<DB>)

input_file = open('/Users/cindykrafft/Desktop/SER-663/striiv-gae-be/device_ids.txt', 'r')
with open('acer_output.txt', 'w') as output_file:
    csvwriter = csv.writer(output_file, delimiter='\t')
    csvwriter.writerow(['Height', 'Weight', 'Gender'])
    count = 0
    for line in input_file:
        count += 1
        try:
            serial = line.strip().upper()
            serial = "'" + serial + "'"
            cur = db.cursor()
            query = "select profile.height, profile.weight, striiv_user.gender from profile \
                        inner join profile_pebble on profile.id = profile_pebble.profile_id \
                        inner join pebble on pebble.id = profile_pebble.pebble_id \
                        inner join striiv_user on striiv_user.id = profile.striiv_user_id \
                        where pebble.serial_number = %s and \
                        profile_pebble.status = 'ACTIVE'" % serial
            cur.execute(query)
            row = cur.fetchone()
            height = row[0]
            weight = row[1]
            gender = str(row[2])
            height_in_inches = str(float(height) * 0.393701)
            weight_in_lbs = str(float(weight) * 2.20462)
            csvwriter.writerow([height_in_inches, weight_in_lbs, gender])
        except TypeError:
            if count > 20000:
                print count
            continue

women_weight = []
women_height = []
men_height = []
men_weight = []
with open('acer_output.txt', 'r') as input_file:
    csvreader = csv.reader(input_file, delimiter='\t')
    women_count = 0
    men_count = 0
    for line in csvreader:
        if line[0] == "Height":
            continue
        if line[2] == '1':
            women_count += 1
            women_height.append(line[0])
            women_weight.append(line[1])
        elif line[2] == '2':
            men_count += 1
            men_height.append(line[0])
            men_weight.append(line[1])
    print "women count: ", women_count
    print "men count: ", men_count

women_weight_bins = {}
women_height_bins = {}
men_weight_bins = {}
men_height_bins = {}

for weight in women_weight:
    weight = math.ceil(float(weight))
    weight = (int(weight) // 5) * 5
    if weight not in women_weight_bins:
        women_weight_bins[weight] = 0
    women_weight_bins[weight] += 1

for weight in men_weight:
    weight = math.ceil(float(weight))
    weight = (int(weight) // 5) * 5
    if weight not in men_weight_bins:
        men_weight_bins[weight] = 0
    men_weight_bins[weight] += 1

for height in women_height:
    height = math.floor(float(height))
    if height not in women_height_bins:
        women_height_bins[height] = 0
    women_height_bins[height] += 1

for height in men_height:
    height = math.floor(float(height))
    if height not in men_height_bins:
        men_height_bins[height] = 0
    men_height_bins[height] += 1

height_x_axis = set()
women_height_y_axis = []
men_height_y_axis = []

for height in women_height_bins:
    if height < 1000:
        height_x_axis.add(height)
    else:
        print height


for height in men_height_bins:
    if height < 1000:
        height_x_axis.add(height)
    else:
        print height


height_x_axis = list(height_x_axis)

for height in height_x_axis:
        if height not in women_height_bins:
            women_height_y_axis.append(0)
        else:
            women_height_y_axis.append(women_height_bins[height])

for height in height_x_axis:
    if height not in men_height_bins:
        men_height_y_axis.append(0)
    else:
        men_height_y_axis.append(men_height_bins[height])

height_x_axis_array = numpy.asarray(height_x_axis)

width = .5
fig, ax = plt.subplots()
rects1 = ax.bar(height_x_axis_array, men_height_y_axis, width, color='b')
rects2 = ax.bar(height_x_axis_array+width, women_height_y_axis, width, color='r')
ax.set_xlabel('Height in inches')
ax.set_ylabel('Number of users')
ax.set_title('Height by gender')
ax.legend((rects1[0], rects2[0]), ('Men', 'Women'))

plt.show()
